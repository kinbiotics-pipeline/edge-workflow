#!/bin/bash

[[ $(curl -V) ]] || { echo 'curl not installed, please install first or setup databases manually' ; exit 1; } 

[ ! -d db ] && mkdir db ;

[ ! -d db ] && echo 'Failed to create database directory ./db.' && exit 1;

dbdir="$(realpath db)";

#setup card database

tmpdir="$(mktemp -d)";

cd "${tmpdir}" || exit 1;

curl https://card.mcmaster.ca/latest/data > card.tar.bz2

tar xf card.tar.bz2 || { echo "could not extract archive, is lbzip2 installed?"; exit 1; }

cp nucleotide_fasta_protein_homolog_model.fasta "${dbdir}/card_nucleotide_fasta_protein_homolog_model.fasta"

cd "${dbdir}/.."

rm -r "${tmpdir}"

# setup sepcies

curl https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/species/species.fna > "${dbdir}/species.fna"
