KINBIOTICS Edge Workflow
========================

The KINBIOTICS edge workflow is supposed to run on the same machine as the Oxford Nanopore Technologies [MinKNOW](https://help.nanoporetech.com/en/articles/6628124-what-does-minknow-do) Software with attached Flongle or MinION sequencing device.

The workflow should be run immediately after the MinKNOW sequencing run has begun. The output folder of the analyzied run has to be specified as command line argument, together with the [cloud API](https://gitlab.ub.uni-bielefeld.de/kinbiotics-pipeline/cloud-workflow-api) server name. All later analysis steps are then started automatically.

To setup the workflow for the first time, the `setup_databases.sh` script can be run, which downloads the relevant species and resistance databases. Also [nextflow](https://www.nextflow.io/) and [Docker](https://docker.io) have to be installed. The worklfow has been tested with Nextflow 22.10.7 (with OpenJDK 17.0.7) and  Docker 20.10.21.

The execution can then be started with the command `nextflow run main.nf --minknowdir '/path/to/minkow/run/dir/' --cloud_api_server CLOUD_API_SERVER`. Optionally, if the [database](https://gitlab.ub.uni-bielefeld.de/kinbiotics-pipeline/database-api) server is run on a different machine, it's name should also be specified through the `--db_api_server` parameter.
