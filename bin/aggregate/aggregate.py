#!/usr/bin/env python3

import pandas as pd
import sys


def main():

    # get csv as first argument
    csv = pd.read_csv(sys.argv[1])

    # bins to bin the data into
    percentage_bins = [0, 0.75, 0.85, 0.90, 0.95, 1]
    percentage_label = ['0-75', '76-85', '85-90', '90-95', '95-100']

    # bin percentages, so this does not have to happen on every database query
    col = 'match_percentage'
    csv[col+'_bin'] = pd.cut(csv[col], bins=percentage_bins, labels=percentage_label)

    # calculate coverage
    coverage = csv['alignment_block_length'] / csv[['target_length', 'query_length']].min(axis=1)
    coverage_sign = (1-coverage <= 0).replace({True: -1, False: 1})
    length_score = (coverage-1) * coverage_sign + 1

    # multiply coverage and identity for the edge score 
    csv['score'] = length_score * csv['match_percentage']

    # output result to stdout as csv
    csv.to_csv(sys.stdout, index=False)


if __name__ == '__main__':
    main()
