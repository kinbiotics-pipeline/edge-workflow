
/*
    Upload a file to an API
*/
process put_file {

    input:
        tuple path(data), val(project)
        val api_server
        val api_endpoint

    container 'quay.io/dlehmenk/csvkit-curl-jq:latest'
    
    script:
    """
    curl --upload-file "${data}" http://${api_server}/${project}/${api_endpoint}/${data}
    """
}

/*
    Upload a file to the base project URL
*/
process put_file_base {

    input:
        tuple path(data), val(project)
        val api_server

    container 'quay.io/dlehmenk/csvkit-curl-jq:latest'
    
    script:
    """
    curl --upload-file "${data}" http://${api_server}/${project}/${data}
    """
}

/*
    Post the final resulting csv as JSON to the configured API
*/
process post_csv_to_json_api {

    input:
        tuple path(data), val(project)
        val api_server
        val api_endpoint

    container 'quay.io/dlehmenk/csvkit-curl-jq:latest'
    
    script:
    """
    # In the second pipe the source field is added to the converted json for every entry
    csvjson "${data}" | jq '.[] += { "source": "$params.machine_id", "count": 1  }' | curl --header "Content-Type: application/json" --request POST --data @- http://${api_server}/${project}/${api_endpoint}
    """
}


/*
    Calculate a score for each match
*/
process score {

    input:
       tuple path(paflikecsv), val(prefix)

    output:
        tuple path("${paflikecsv}_scored.csv"), val(prefix), emit: scored_paflike

    publishDir "${params.publishdir}/${prefix}"
    
    container 'quay.io/biocontainers/pandas:1.5.2'

    """
    aggregate.py "${paflikecsv}" > "${paflikecsv}_scored.csv"
    """
}


